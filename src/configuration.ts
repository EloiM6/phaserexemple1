import level1 from "./scenes/level1";
import LoadFiles from "./scenes/loadFiles";
import menu from "./scenes/menu";
import level1Text from "./scenes/level1Text";

const config = {
    type: Phaser.AUTO,
    backgroundColor: '#125555',
    width: 800,
    height: 600,
    // A les escenes s'ha de posar l'ordre en què es
    // carreguen els diferents fitxers
    scene: [LoadFiles, menu, level1, level1Text]
};
export default config;
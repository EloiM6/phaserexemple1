import Properties from "../properties";

export default class menu extends Phaser.Scene{
    // Es creen 2 variables privades per controlar l'alçada
    // i l'amplada
    private amplada : number;
    private altura : number;

    constructor(){
        super(Properties.Scenes.Menu);
    }
    // A vegades no podem associar valors al constructor perquè aquests encara són nuls.
    // Per exemple, encara no tinguem inicialitzat el jugador. Per aquest motiu
    // es prefereix inicialitzar variables a la inicialització de l'objecte.
    init (){
        this.altura = this.cameras.main.height;
        this.amplada = this.cameras.main.width;
    }

    //  A la creació de l'escena afegim elements que necessitem
    // No ho fem al constructor, ni al init sinó a la creació de l'escena.
    // Això és així, perquè l'escena no existeix encara .
    create ()
    {
        const logo = this.add.image(this.amplada/2, 70, 'logo1');
        // El mètode setInteractive ens permet fer clic al text.
        const jugarTXT : Phaser.GameObjects.Text = this.add.text(50,
            this.altura/5, "Opció menú 1",
            {fontSize:"12px", color:"yellow"}).setInteractive();

        this.canviscene(jugarTXT,Properties.Scenes.Level1)
        this.canviscene(jugarTXT,Properties.Scenes.Level1TXT, true)
    }
    // Al clicar el text, li hem d'indicar cap a on el volem enviar
    canviscene(jugarTXT : Phaser.GameObjects.Text, newscene: string, bringtop ?:boolean ){
        // pointerdown és l'esdeveniment de quan es dona clic
        // al text o algun lloc de la pàgina
        jugarTXT.on('pointerdown',() => {
            this.scene.start(newscene);
            if (bringtop){
                this.scene.bringToTop(newscene);
            }

        })
    }
}
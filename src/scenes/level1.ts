import * as events from "events";
import Properties from "../properties";

export default class level1 extends Phaser.Scene
{
    private amplada : number;
    private altura : number;
    private score : number;
    private info : Phaser.GameObjects.Text;
    constructor ()
    {
        super(Properties.Scenes.Level1);
    }
    init (){
        this.altura = this.cameras.main.height;
        this.amplada = this.cameras.main.width;
        this.score = 0;
    }
    preload ()
    {
        // Això ja es fa a la càrrega
        //this.load.image('logo11', 'assets/phaser3-logo.png');
    }
    create ()
    {
        // Fons del joc
        this.add.image(400, 300, 'bg');
        const logo = this.add.image(400, 70, 'logo1');
        // Aquest text ens serveix per tornar a la pantalla inicial
        // En aquest cas la pantalla inicial és l'assert menu.ts.
        const jugarTXT2 : Phaser.GameObjects.Text = this.add.text(50,
            this.altura/5, Properties.Menu.Retorn,
            {fontSize:"12px", color:"red"}).setInteractive();
        // Afegim un text per indicar que s'ha obert una altra
        // escena.
        const jugarTXT : Phaser.GameObjects.Text = this.add.text(150,
            this.altura/5, Properties.Menu.Opcio1,
            {fontSize:"12px", color:"yellow"})

        for (var i = 0; i < 30; i++)
        {
            let x:number = Phaser.Math.Between(0, 800);
            let y:number = Phaser.Math.Between(this.altura/3, 600);

            let box: Phaser.GameObjects.Image = this.add.image(x, y, 'crate');

            //  Make them all input enabled
            box.setInteractive();
        }

        this.canviscene(jugarTXT2,Properties.Scenes.Menu);

        this.input.on('gameobjectup', this.clickHandler, this);
    }
    clickHandler(pointer, box: Phaser.GameObjects.Image):void
    {
        if (box.texture.key == 'crate') {
            //  Disable our box
            box.input.enabled = false
            box.setVisible(false)
            this.score += 10;
            // Inicialitzem la variable global score
            this.registry.set(Properties.Var_Global.Marcador,this.score);
            // Llencem l'esdeveniment chageScore
            // per canviar el text
            this.events.emit(Properties.Events.CanviPuntuacio);

        }
    }
    // Al clicar el text, li hem d'indicar cap a on el volem enviar
    canviscene(jugarTXT : Phaser.GameObjects.Text, newscene: string){
        // pointerdown és l'esdeveniment de quan es dona clic
        // al text o algun lloc de la pàgina
        jugarTXT.on('pointerdown',() => {
            this.scene.start(newscene);
            // Parem l'escena
            this.scene.stop(Properties.Scenes.Level1TXT);
        })

    }
}
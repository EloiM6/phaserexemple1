export default class LoadFiles extends Phaser.Scene{
    // Es creen dos variables privades per la barra de desplaçament
    private LoadBar : Phaser.GameObjects.Graphics;
    private ProgressBar : Phaser.GameObjects.Graphics;

    // Constructor per carrergar els gràfics
    constructor(){
        super("LoadFiles");
    }
    preload() : void{
        this.cameras.main.setBackgroundColor(0x000000);
        this.createBars();
        // S'afegeix un listener mentre es carrega
        // Mentre es carrega es veu com una barra que s'omple
        this.load.on(
            'progress',
            function (value:number){
                this.ProgressBar.clear();
                this.ProgressBar.fillStyle(0x88e453, 1);
                this.ProgressBar.fillRect(
                    this.cameras.main.width / 4,
                    (this.cameras.main.height / 2) - 16,
                    (this.cameras.main.width / 2) * value,
                    16
                );
            },
            this
        )
        // Listener per quan s'hagin completat la càrrega de tots els fitxers
        this.load.on(
            'complete',
            function (){
                this.scene.start("menu");
            },
            this
        );
        this.load.image('bg', 'assets/sky4.png');
        this.load.image('crate', 'assets/crate.png');
        for (let i = 1; i < 1001; i++){
            this.load.image('logo'+i, 'assets/phaser3-logo.png');
        }
    }
    // Mètode per crear la barra de progrés
    private createBars(){
        this.LoadBar = this.add.graphics();
        this.LoadBar.fillStyle(0xffffff, 1)
        this.LoadBar.fillRect(
            (this.cameras.main.width / 4) - 2,
            (this.cameras.main.height /2) - 18,
            (this.cameras.main.width / 2)  + 4,
            20
        );
        this.ProgressBar = this.add.graphics();
    }

}
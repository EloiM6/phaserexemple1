import Properties from "../properties";

export default class level1Text extends Phaser.Scene{
    private scoreTxt:Phaser.GameObjects.Text;
    private amplada : number;
    private altura : number;

    constructor ()
    {
        super(Properties.Scenes.Level1TXT);
    }
    init (){
        this.altura = this.cameras.main.height;
        this.amplada = this.cameras.main.width;

    }
    preload(){

    }

    create ():void
    {
        // Obtenim l'escena del joc per anar imprimint el text
        const level1 : Phaser.Scene = this.scene.get(Properties.Scenes.Level1);
        // Afegeixo un esdeveniment a gestor d'esdeveniments
        // per tractar el text
        // This s'ha de posar per situar on ens trobem i on s'ha de cridar
        // l'esdeveniment
        level1.events.on(Properties.Events.CanviPuntuacio, this.changeScore, this)
        // Text que indica el marcador del joc
        this.scoreTxt = this.add.text(50, this.altura/4, 'Score: 0',
            { font: '24px Arial', color: 'black' });
    }
    private changeScore() : void {
        // Utilitzem string.pad per posar una màscara
        this.scoreTxt.text = "Score: " +
            Phaser.Utils.String.Pad(this.registry.get(Properties.Var_Global.Marcador),
                3, '0', 1);
    }

}
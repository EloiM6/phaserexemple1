import 'phaser';
import config from './configuration'

export class game extends Phaser.Game {
    // S'afegeix el constructor l'script de configuració
    constructor(configuration : Phaser.Types.Core.GameConfig){
        super(configuration);
    }
}

window.addEventListener('load', () => {
    const joc = new game(config);
});